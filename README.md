Embed image as pixel data `&[u8]` into binary at build time.

You don't have to add `buildtime-png` as a `dependency` - just add it as a `build-dependency`.

See code in `sample` folder for a sample usage.
