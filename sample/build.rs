use buildtime_png;

fn main () {
  buildtime_png::Builder::new()
    .include_png("images/left_ptr.png", "left_ptr")
    .include_png("images/red.png", "red")
    .emit_source_file_at("src/images.rs");
}
